@extends('layouts.app')

@section('title', $pseudo . ' nom de page')

@push('styles')
    <link href="{{ plugin_asset('player-flash', "css/reset.css") }} " rel="stylesheet">
    <link href="{{ plugin_asset('player-flash', "css/style.css") }} " rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css"/>
@endpush

@section('content')
    <main>
        <div id="profile">
            <div class="header">
                <div class="wrapper">
                    <div class="overview">
                        <div class="avatar">
                            <img src="{{ \Azuriom\Plugin\PlayerFlash\Helpers\MojangAPI::getskinBodyUrl($user->uuid) }}"
                                 alt="skin user"/>
                        </div>
                        <div class="info">
                            <h1>{!! Azuriom\Plugin\PlayerFlash\Helpers\OtherTools::parseMinecraftColors($user->prefix . $pseudo) !!} </h1>
                            <p class="rank {{ strtolower($user->rank) }}">{{ $user->rank }}</p>
                        </div>
                    </div>
                    <div class="status {{ $isOnline ? 'online' : "" }}">
                        <h4>{{ $isOnline ? "En ligne" : "Hors ligne" }}</h4>
                        <p>{{ $isOnline ? "Depuis" : "Dernière visite" }}
                            {{ $isOnline ? Azuriom\Plugin\PlayerFlash\Helpers\OtherTools::ageSecond(\Carbon\Carbon::now()->timestamp -
\Carbon\Carbon::createFromTimestampMs($user->lastConnection)->timestamp) : \Carbon\Carbon::createFromTimestampMs($user->lastConnection)->diffForHumans() }}</p>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="wrapper">
                    <div class="left-heavy">
                        <div class="game">
                            <h3 class="sheep">Sheepwars</h3>
                            <div class="image">
                                <img src="{{ plugin_asset('player-flash', "img/sheepwars.png") }}"/>
                            </div>
                            <ul>
                                <li>
                                    <p>Victoires</p>
                                    <span>{{ $stat["SHEEPWARS"] ? $stat["SHEEPWARS"]->victory : "0" }}</span>
                                </li>
                                <li>
                                    <p>Défaites</p>
                                    <span>{{ $stat["SHEEPWARS"] ? $stat["SHEEPWARS"]->defeats : "0" }}</span>
                                </li>
                                <li>
                                    <p>Kills</p>
                                    <span>{{ $stat["SHEEPWARS"] ? $stat["SHEEPWARS"]->kills : "0" }}</span>
                                </li>
                                <li>
                                    <p>Morts</p>
                                    <span>{{ $stat["SHEEPWARS"] ? $stat["SHEEPWARS"]->deaths : "0" }}</span>
                                </li>
                                <li>
                                    <p>Parties Joués</p>
                                    <span>{{ $stat["SHEEPWARS"] ? $stat["SHEEPWARS"]->nb_played : "0"}}</span>
                                </li>
                                <li>
                                    <p>Temps De Jeu</p>
                                    <span>{{ Azuriom\Plugin\PlayerFlash\Helpers\OtherTools::ageMs($stat["SHEEPWARS"] ? $stat["SHEEPWARS"]->timeplayed : "0") }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="game">
                            <h3 class="murder">Murder</h3>
                            <div class="image">
                                <img src="{{ plugin_asset('player-flash', "img/murder.png") }}"/>
                            </div>
                            <ul>
                                <li>
                                    <p>Victoires</p>
                                    <span>{{ $stat["MURDER"] ? $stat["MURDER"]->victory : "0" }}</span>
                                </li>
                                <li>
                                    <p>Défaites</p>
                                    <span>{{ $stat["MURDER"] ? $stat["MURDER"]->defeats : "0" }}</span>
                                </li>
                                <li>
                                    <p>Kills</p>
                                    <span>{{ $stat["MURDER"] ? $stat["MURDER"]->kills : "0"}}</span>
                                </li>
                                <li>
                                    <p>Morts</p>
                                    <span>{{ $stat["MURDER"] ? $stat["MURDER"]->deaths : "0" }}</span>
                                </li>
                                <li>
                                    <p>Parties Joués</p>
                                    <span>{{ $stat["MURDER"] ? $stat["MURDER"]->nb_played : "0"}}</span>
                                </li>
                                <li>
                                    <p>Temps De Jeu</p>
                                    <span>{{ Azuriom\Plugin\PlayerFlash\Helpers\OtherTools::ageMs($stat["MURDER"] ? $stat["MURDER"]->timeplayed : "0") }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="game">
                            <h3 class="sumo">Sumo</h3>
                            <div class="image">
                                <img src="{{ plugin_asset('player-flash', "img/sumo.png") }}"/>
                            </div>
                            <ul>
                                <li>
                                    <p>Victoires</p>
                                    <span>{{ $stat["SUMO"] ? $stat["SUMO"]->victory : "0" }}</span>
                                </li>
                                <li>
                                    <p>Défaites</p>
                                    <span>{{ $stat["SUMO"] ? $stat["SUMO"]->defeats : "0" }}</span>
                                </li>
                                <li>
                                    <p>Kills</p>
                                    <span>{{ $stat["SUMO"] ? $stat["SUMO"]->kills : "0" }}</span>
                                </li>
                                <li>
                                    <p>Morts</p>
                                    <span>{{ $stat["SUMO"] ? $stat["SUMO"]->deaths : "0"}}</span>
                                </li>
                                <li>
                                    <p>Parties Joués</p>
                                    <span>{{ $stat["SUMO"] ? $stat["SUMO"]->nb_played : "0" }}</span>
                                </li>
                                <li>
                                    <p>Temps De Jeu</p>
                                    <span>{{ Azuriom\Plugin\PlayerFlash\Helpers\OtherTools::ageMs( $stat["SUMO"] ? $stat["SUMO"]->timeplayed : "0") }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="game">
                            <h3 class="lg-uhc">Loup Garou UHC</h3>
                            <div class="image">
                                <img src="{{ plugin_asset('player-flash', "img/lguhc.png") }}"/>
                            </div>
                            <ul>
                                <li>
                                    <p>Victoires</p>
                                    <span>{{ $stat["HOST_LG"] ? $stat["HOST_LG"]->victory : "0" }}</span>
                                </li>
                                <li>
                                    <p>Défaites</p>
                                    <span>{{ $stat["HOST_LG"] ? $stat["HOST_LG"]->defeats : "0" }}</span>
                                </li>
                                <li>
                                    <p>Kills</p>
                                    <span>{{ $stat["HOST_LG"] ? $stat["HOST_LG"]->kills : "0" }}</span>
                                </li>
                                <li>
                                    <p>Morts</p>
                                    <span>{{ $stat["HOST_LG"] ? $stat["HOST_LG"]->deaths : "0"}}</span>
                                </li>
                                <li>
                                    <p>Parties Joués</p>
                                    <span>{{ $stat["HOST_LG"] ? $stat["HOST_LG"]->nb_played : "0" }}</span>
                                </li>
                                <li>
                                    <p>Temps De Jeu</p>
                                    <span>{{ Azuriom\Plugin\PlayerFlash\Helpers\OtherTools::ageMs( $stat["HOST_LG"] ? $stat["HOST_LG"]->timeplayed : "0") }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="game">
                            <h3 class="arena">Aréna</h3>
                            <div class="image">
                                <img src="{{ plugin_asset('player-flash', "img/arena.png") }}"/>
                            </div>
                            <ul>
                                <li>
                                    <p>Kills</p>
                                    <span>{{ $stat["ARENA"] ? $stat["ARENA"]->kills : "0" }}</span>
                                </li>
                                <li>
                                    <p>Morts</p>
                                    <span>{{ $stat["ARENA"] ? $stat["ARENA"]->deaths : "0" }}</span>
                                </li>
                                <li>
                                    <p>Temps de jeu</p>
                                    <span>{{ Azuriom\Plugin\PlayerFlash\Helpers\OtherTools::ageMs($stat["ARENA"] ? $stat["ARENA"]->timeplayed : "0") }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="right-light">
                        <div class="title yellow">
                            <h2>Statistiques Globales</h2>
                        </div>
                        <ul class="stats">
                            <li>
                                <i class="fa fa-calendar blue"></i>
                                <p>Joueur depuis</p>
                                <span>{{ \Carbon\Carbon::createFromTimestampMs($user->joinedAt)->format("d/m/Y") }}</span>
                            </li>
                            <li>
                                <i class="fa fa-clock-o mint"></i>
                                <p>Temp de jeu</p>
                                <span>{{ Azuriom\Plugin\PlayerFlash\Helpers\OtherTools::ageMs($user->timeplayed) }}</span>
                            </li>
                            <li>
                                <i class="fa fa-address-book-o orange"></i>
                                <p>Amis</p>
                                <span>{{ count($user->friends) }}</span>
                            </li>
                            <li>
                                <i class="fa fa-star magenta"></i>
                                <p>Level</p>
                                <span>{{ $user->level }}</span>
                            </li>
                            <li>
                                <i class="fa fa-trophy gray"></i>
                                <p>Achievements</p>
                                <span>(Prochainement)</span>
                            </li>
                        </ul>
                        @if(in_array("SUFFIXFYZ", $user->availableSuffix) && count($user->availableSuffix) > 1 )
                            <div class="title blue">
                                <h2>Badges</h2>
                            </div>
                            <ul class="trophies">
                                @if(in_array("SUFFIXFYZ", $user->availableSuffix))
                                    <li class="yellow" style="background-color: #e6ac30;">
                                        <i style="color: #fbf546;" class="fas fa-code"></i>
                                        <div class="hover">
                                            <h5>Badge Développeur</h5>
                                            <p>Aide au développement du serveur RDC Games.</p>
                                        </div>
                                    </li>
                                @endif
                                @if(in_array("SUFFIX3", $user->availableSuffix))
                                    <li class="yellow" style="background-color: #FA5354;">
                                        <i style="color: #cc4242;" class="fas fa-heart"></i>
                                        <div class="hover">
                                            <h5>Badge Aide</h5>
                                            <p>Aide significative du serveur RDC Games.</p>
                                        </div>
                                    </li>
                                @endif
                                @if(in_array("SUFFIX1", $user->availableSuffix))
                                    <li class="yellow" style="background-color: #00b300;">
                                        <i style="color: #007F00;" class="fas fa-bug"></i>
                                        <div class="hover">
                                            <h5>Badge Bug Hunter</h5>
                                            <p>Aide à la recherche de bug du serveur RDC Games.</p>
                                        </div>
                                    </li>
                                @endif
                                @if(in_array("SUFFIX2", $user->availableSuffix))
                                    <li class="yellow" style="background-color: #b800c4;">
                                        <i style="color: #eb00fa;" class="fas fa-plane"></i>
                                        <div class="hover">
                                            <h5>Badge Irl</h5>
                                            <p>Rencontre d'un membre du haut staff du serveur RDC Games.</p>
                                        </div>
                                    </li>
                                @endif
                                @if(in_array("SUFFIX4", $user->availableSuffix))
                                    <li class="yellow" style="background-color: #00c1c8;">
                                        <i style="color: #00f6ff;" class="fas fa-hashtag"></i>
                                        <div class="hover">
                                            <h5>Badge Early</h5>
                                            <p>Achat sur la boutique avant le XX/XX/2021.</p>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </main>
@endsection
