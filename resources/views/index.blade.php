@extends('layouts.app')

@section('title', 'Plugin home')

@push('styles')
    <link href="{{ plugin_asset('player-flash', "css/reset.css") }} " rel="stylesheet">
    <link href="{{ plugin_asset('player-flash', "css/style.css") }} " rel="stylesheet">

@endpush

@section('content')
    <main>
        <div id="community">
            <div class="header">
                <div class="background">
                    <div class="wrapper">
                        <img src="https://crafatar.com/renders/body/c068c6ff87cf4732bbb9a59c4eecce22" />
                        <img src="https://crafatar.com/renders/body/f934c5ba38a54ff3845cc894bab20c26" />
                        <img src="https://crafatar.com/renders/body/7cee0ed0ccc9473ab910e1c6d979709c" />
                        <img src="https://crafatar.com/renders/body/b43821e2080e49779c9ee65973b3e3e2" />
                        <img src="https://crafatar.com/renders/body/b1a46c10c2cb4be78ebc8145e6eb4011" />
                        <img src="https://crafatar.com/renders/body/53139a1970c0406a85f6fb6acabddc1b" />
                        <img src="https://crafatar.com/renders/body/0c5662834de14642bee1ac362f38781d" />
                        <img src="https://crafatar.com/renders/body/32c3fc040087455fb785b8186c871c5a" />
                    </div>
                </div>
                <div class="overlay">
                    <div class="wrapper">
                        <h1><em>RDC GAMES</em> Communauté</h1>
                        <div class="search">
                            <div class="textbox">
                                <i class="fa fa-search"></i>
                                <input type="text" name="rdc-player-search" id="rdc-player-search" placeholder="Rechercher un joueur..." />
                            </div>
                            <a class="button" href="{{ route("player.home") }}" id="rdc-player-submit">Rechercher</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="wrapper">
                    <div class="left-light">
                        <div class="title">
                            <h2>Rejoins notre discord</h2>
                        </div>
                        <iframe src="https://discord.com/widget?id=581946738831786025&theme=dark" width="350"
                                height="500" allowtransparency="true" frameborder="0"
                                sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>
                    </div>
                    <div class="right-heavy">
                        <div class="title gray">
                            <h2>Vidéos récentes</h2>
                        </div>
                        <div class="video-player">
                            <div class="current" id="yt-player">
                                <iframe width="760" height="428" src="https://www.youtube.com/embed/orVPi9_cbSc"
                                        title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@push("footer-scripts")
    <script>
        const element = document.getElementById("rdc-player-submit");
        element.addEventListener("click", (event) => {
            event.preventDefault()
            window.location.href = '{{ route("player.home") }}' + "/" + document.getElementById("rdc-player-search").value;
        })
    </script>
@endpush
