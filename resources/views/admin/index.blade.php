@extends('admin.layouts.admin')

@section('title', 'Player plugin')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="{{ route("player.admin.update") }}" method="POST">
                @csrf
                <h3>Database</h3>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="host">Host</label>
                        <input type="text" class="form-control" id="host" name="host" value="{{ $database["host"] }}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="port">Port</label>
                        <input type="text" class="form-control" id="port" name="port" value="{{ $database["port"] }}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="dbname">DBName</label>
                        <input type="text" class="form-control" id="dbname" name="dbname" value="{{ $database["dbname"] }}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username" value="{{ $database["username"] }}">
                    </div>
                    <div class="form-group col">
                        <label for="password">Password</label>
                        <input type="text" class="form-control" id="password" name="password" value="{{ $database["password"] }}">
                    </div>
                </div>
                <h3>Minecraft</h3>
                <div class="form-group">
                    <label for="ip">Address IP</label>
                    <input type="text" class="form-control" id="ip" name="ip" placeholder="hypixel.net" value="{{ $minecraft }}">
                </div>

                <button type="submit" class="btn btn-primary">
                    <i class="fas fa-save"></i>
                </button>

            </form>
        </div>
    </div>
@endsection
